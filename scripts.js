const openClose = document.querySelector('.openClose');
const general_navbar = document.querySelector('.general_navbar');
const Btn = document.querySelector('.btn_one');
const btntwo = document.querySelector('.btn_two');
const btn_thee =  document.querySelector('.btn_thee');
const btn_for = document.querySelector('.btn_for');
const btn_five = document.querySelector('.btn_five');
const container = document.querySelector('.card_item');
const btn_six = document.querySelector('.btn_six');

openClose.addEventListener('click' , e => {
    e.preventDefault();
    general_navbar.classList.toggle("active");
})
const getPass = cb => {
    const xhr = new XMLHttpRequest();
    xhr.open('GET' , 'https://jsonplaceholder.typicode.com/posts');
    xhr.addEventListener('load' , () => {
    const res = JSON.parse(xhr.response);
    cb(res);
});
xhr.addEventListener('error' , err => {
    console.log(err);
    console.log('error')
})
xhr.send();
}
Btn.addEventListener('click' , e=> {
e.preventDefault();

    getPass(res => {
        container.innerHTML = res.map(item => cardtemp(item)).join('');
    })
})
function cardtemp(post){
    return `
        <div class="card">
            <div class="card-header">
                <h1>${post.title}</h1>
            </div>
            <div class="card-body">
                <p>${post.body}</p>
            </div>
        </div>
    `
}
const getPassTwo = cb => {
    const xhrtwo = new XMLHttpRequest();
    xhrtwo.open('GET' , 'https://jsonplaceholder.typicode.com/comments');
    xhrtwo.addEventListener('load' , () => {
    const restwo = JSON.parse(xhrtwo.response);
    cb(restwo);
});
xhrtwo.addEventListener('error' , err => {
    console.log(err);
    console.log('error')
})
xhrtwo.send();
}
btntwo.addEventListener('click' , e=> {
    e.preventDefault();
    
        getPassTwo(res => {
            container.innerHTML = res.map(item => cardtempTwo(item)).join('');
        })
    })
function cardtempTwo(post){
    return `
        <div class="card-second">
            <div class="card-header-second">
                <h1>${post.name}</h1>
            </div>
            <div class="card-body-second">
                <p>${post.body}</p>
            </div>
            <div class="card-email">
                <p>${post.email}</p>
            </div>
        </div>
    `
}
const getPassThee = cb => {
    const xhrthree = new XMLHttpRequest();
    xhrthree.open('GET' , 'https://jsonplaceholder.typicode.com/albums');
    xhrthree.addEventListener('load' , () => {
    const resthree = JSON.parse(xhrthree.response);
    cb(resthree);
});
xhrthree.addEventListener('error' , err => {
    console.log(err);
    console.log('error')
})
xhrthree.send();
}
btn_thee.addEventListener('click' , e=> {
    e.preventDefault();
    getPassThee(res => {
            container.innerHTML = res.map(item => cardtempThree(item)).join('');
        })
    })
function cardtempThree(post){
    return `
    <div class="card-three">
        <div class="card-body-three">
            <p>${post.title}</p>
        </div>
    </div>
    `
}
const getPassFor = cb => {
    const xhrfor = new XMLHttpRequest();
    xhrfor.open('GET' , 'https://jsonplaceholder.typicode.com/photos');
    xhrfor.addEventListener('load' , () => {
    const restfor = JSON.parse(xhrfor.response);
    cb(restfor);
});
xhrfor.addEventListener('error' , err => {
    console.log(err);
    console.log('error')
})
xhrfor.send();
}
btn_for.addEventListener('click' , e=> {
    e.preventDefault();
    getPassFor(res => {
            container.innerHTML = res.map(item => cardtempfor(item)).join('');
        })
    })
function cardtempfor(post){
    return `
    <div class="card-for">
        <div class="card-bodys-img">
            <img src="${post.thumbnailUrl}">
        </div>
        <div class="card-body-for">
            <p>${post.title}</p>
        </div>
    </div>
    `
}
const getPassFive = cb => {
    const xhrfive = new XMLHttpRequest();
    xhrfive.open('GET' , 'https://jsonplaceholder.typicode.com/todos');
    xhrfive.addEventListener('load' , () => {
    const restfive = JSON.parse(xhrfive.response);
    cb(restfive);
});
xhrfive.addEventListener('error' , err => {
    console.log(err);
    console.log('error')
})
xhrfive.send();
}
btn_five.addEventListener('click' , e=> {
    e.preventDefault();
    getPassFive(res => {
            container.innerHTML = res.map(item => cardtempfive(item)).join('');
        })
    })
function cardtempfive(post){
    return `
    <div class="card-five">
        <div class="card-body-five">
            <p>${post.title}</p>
        </div>
    </div>
    `
}
const getPassSix = cb => {
    const xhrsix = new XMLHttpRequest();
    xhrsix.open('GET' , 'https://jsonplaceholder.typicode.com/users');
    xhrsix.addEventListener('load' , () => {
    const restsix = JSON.parse(xhrsix.response);
    cb(restsix);
});
xhrsix.addEventListener('error' , err => {
    console.log(err);
    console.log('error')
})
xhrsix.send();
}
btn_six.addEventListener('click' , e=> {
    e.preventDefault();
    getPassSix(res => {
            container.innerHTML = res.map(item => cardtempsix(item)).join('');
        })
    })
function cardtempsix(post){
    return `
    <div class="card-six">
        <div class="card-body-six">
            <p>${post.name}</p>
            <p>${post.username}</p>
            <p>${post.email}</p>
            <p>${post.address.street}</p>
            <p>${post.address.suite}</p>
            <p>${post.address.city}</p>
            <p>${post.address.zipcode}</p>
            <p>${post.address.geo.lat}</p>
            <p>${post.address.geo.lng}</p>
            <p>${post.phone}</p>
            <p>${post.website}</p>
            <p>${post.company.name}</p>
            <p>${post.company.catchPhrase}</p>
            <p>${post.company.bs}</p>
        </div>
    </div>
    `
}